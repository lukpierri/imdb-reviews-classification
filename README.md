# TM&S project - Binary classification of IMDB film reviews

### how to run

1. Download anaconda installer for your platform
2. create a virtual environment with `conda create --name $ENV_NAME`
3. activate it with `conda activate $ENV_NAME`
4. install requirements with `pip install -r requirements.txt`
5. open a shell, navigate to project directory and run `jupyter notebook`
6. run the whole `preprocess.ipynb` notebook which produces `preprocessed.csv` file
7. run the `classifiers.ipynb` produce the classification report. It gives the comparison\\
between classifiers in term of accuracy

